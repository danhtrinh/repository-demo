## How to install
1. php composer.phar install
2. .env.example -> .env
3. import respository-demo-one.sql vào phpmyadmin

## Note
1. Các file repository nằm trong app/Repositories
2. Đăng ký sử dụng các file repositories trong app/Providers/AppServiceProvider.php
3. Các file controller nằm trong app/Http/Controllers
4. Các file model nằm trong app

## Tài liệu
1. SQL: https://allaravel.com/laravel-tutorials/lam-viec-voi-co-so-du-lieu-trong-laravel/
2. Import/export excel: https://www.youtube.com/watch?v=Kwk9MNAhzxk&list=PL1aMeb5UP_PF_v42oDqvaGn5eiRD9laUD
3. Repository: https://kipalog.com/posts/Repository-Pattern-trong-Laravel
4. Repository: https://viblo.asia/p/laravel-design-patterns-series-repository-pattern-part-3-ogBG2l1ZRxnL