<form  action="{{ URL('excel-import') }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
	<label>Import file</label>
	<input type="file" name="file">
	<button type="submit">Submit and Import</button>	
</form>
<a href="{{ URL('excel-export') }}">Export</a>