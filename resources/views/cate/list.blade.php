<table>
	<tr>
		<td>STT</td>
		<td>Name</td>
		<td>Action</td>
	</tr>
	@foreach($cate as $index => $item)
		<tr>
			<td>{{ $index + 1 }}</td>
			<td>{{ $item->name }}</td>
			<td>
				<a href="#">Edit</a>
				{{-- <a href="{{ URL::route('cates.destroy', ['id'=>$item->id]) }}">Delete</a> --}}
				{!! Form::open(array('route'=>array('cates.destroy',$item->id), 'method'=>'DELETE')) !!}
	                <button type="submit" id="delete" class="btn btn-link">Delete</button>
	            {!! Form::close() !!}
			</td>
		</tr>
	@endforeach
</table>