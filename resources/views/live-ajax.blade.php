

<input type="hidden" name="_token" value="{!! csrf_token() !!}" >
<select name="catelist" form="carform" id="catelist">
  <option value="1">Categories 1</option>
  <option value="2">Categories 2</option>
  <option value="3">Categories 3</option>
  <option value="4">Categories 4</option>
</select>

<div id="productData">
	<h1>SẢN PHẨM</h1>

	@foreach($products as $product)
		<h1>{{ $product->name }}</h1>
		<p>{{ $product->description }}</p>
		<br>
	@endforeach
</div>


<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script type="text/javascript">
	
	$('#catelist').on('change', function() {
		var value = $(this).val();
	 	var _token = $("input[name='_token']").val();
	  	$.ajax({
			url: 'http://localhost:8000' + '/product-by-cate',
			type: 'POST',
			cache: false,
			data: {"_token":_token, "value":value},
			success: function(data) {
				$('#productData').html(data);
			}
		});

	});
</script>