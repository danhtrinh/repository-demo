<?php
// app/Repositories/Eloquents/ProductRepository.php

namespace App\Repositories\Eloquents;
use App\Repositories\Contracts\ProductRepositoryInterface;

use App\Product;
use DB;

class ProductRepository implements ProductRepositoryInterface
{
    public function all()
    {
       	return Product::all();
       	//return DB::select('select * from products');
        
    }

    public function find($id)
    {
        return Product::find($id);
        //return DB::select('select * from products where id = ?', [$id]);
    }
}