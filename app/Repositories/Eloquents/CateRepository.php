<?php
// app/Repositories/Eloquents/ProductRepository.php

namespace App\Repositories\Eloquents;
use App\Repositories\Contracts\CateRepositoryInterface;

use App\Product;
use App\Cate;
use DB;

class CateRepository implements CateRepositoryInterface
{

    private $cate;

    public function __construct() {
        $this->cate = new Cate();
    }

    public function all() {
       	return DB::table('categories')->get()->toArray();
       	//return DB::select('select * from products');  
    }

    public function find($id) {
        //return Product::find($id);
        return DB::select('select * from categories where id = ?', [$id]);
    }

    public function cateProduct() {
        return DB::select('select products.name as products_name, categories.name as categories_name from categories join products on categories.id = products.id_categories');
    }

    public function save(array $data) {
        // return $this->cate->create($data);
        //return $data['txtName'];
        $this->cate->name = $data['txtName'];
        $this->cate->save();
    }

    public function remove($id) {
        $this->cate->destroy($id);
    }
}