<?php
// app/Repositories/Contracts/ProductRepositoryInterface.php

namespace App\Repositories\Contracts;

interface CateRepositoryInterface
{
    public function all();
    public function find($id);
    public function cateProduct();
    public function save(array $data);
    public function remove($id);
}