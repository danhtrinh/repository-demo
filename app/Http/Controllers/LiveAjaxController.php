<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LiveAjaxController extends Controller
{
    public function showProduct() {
    	$products = Db::table('products')->get();
    	return view('live-ajax', compact('products'));
    }

    public function productByCate() {
    	$products = Db::table('products')->where('id_categories', $_POST['value'])->get();
    	return view('live-ajax', compact('products'));
    }
}
