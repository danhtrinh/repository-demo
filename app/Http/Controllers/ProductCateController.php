<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\Contracts\CateRepositoryInterface;

class ProductCateController extends Controller
{	

	protected $productRepository;
	protected $cateRepository;

	public function __construct(ProductRepositoryInterface $productRepository, CateRepositoryInterface $cateRepository)
    {
        $this->productRepository = $productRepository;
        $this->cateRepository = $cateRepository;
    }

    public function allCateProduct() {
    	$product =  $this->productRepository->all();
    	$cate = $this->cateRepository->all();
    	//return "hello";
    	return view('product-cate', compact('product', 'cate'));
    }

    public function showCateProduct() {
    	$cate_product = $this->cateRepository->cateProduct();
    	return view('product-cate', compact('cate_product'));
    }
}
