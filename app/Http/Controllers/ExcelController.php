<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cate;
use App\Product;
use Excel;
use DB;
use Illuminate\Support\Facades\Input;

class ExcelController extends Controller
{
    public function excelImport(Request $request) {
    	if($request->hasFile('file')) {
    		$path = $request->file('file')->getRealPath();
    		$data = Excel::load($path, function($reader){})->get();
    		if(!empty($data) && $data->count()) {
    			foreach ($data as $key => $row) {
    				$cate = new Cate;
    				$cate->name = $row->name;
    				$cate->save();
    			}
    		}
    	}

    	return 'Yeahhh!';
    }
    public function excelExport() {
    	$contact = DB::select('select products.name as products_name, categories.name as categories_name from categories join products on categories.id = products.id_categories');
    	$contact = json_decode(json_encode($contact),true); // chuyển đổi các stdObject sang dạng Array
		// echo "<pre>";
  //   	print_r($contact);
  //   	echo "<pre>";
    	return Excel::create('data_contact', function($excel) use ($contact) {
    		$excel->sheet('mysheet', function($sheet) use ($contact) {
    			$sheet->fromArray($contact);
    		});
    	})->download('xlsx');
    }
}
