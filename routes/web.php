<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('products', 'ProductController');
Route::resource('cates', 'CateController');
Route::get('cate-product', 'ProductCateController@allCateProduct');
Route::get('show-cate-product', 'ProductCateController@showCateProduct');

// Excel

Route::get('excel-form-import', function() {
	return view('excel-import');
});

Route::post('excel-import', 'ExcelController@excelImport');
Route::get('excel-export', 'ExcelController@excelExport');

// Fill live record with ajax
Route::get('live-ajax', 'LiveAjaxController@showProduct');
Route::post('product-by-cate', 'LiveAjaxController@productByCate');

// Repositories
// Route::group(['prefix'=>'cate'], function(){
// 	Route::get('list', '');	
// });
